# Regenerate project

docker-compose build
docker-compose push

# Activate development

Execute `$ docker-compose -f docker-compose.debug.yml up` and then run de debugger "Attach"

# Autocompletion

To activate autocompletion is needed a venv with all the libraries.

```bash
$ python3.8 -m venv ./venv
$ source ./venv/bin/activate
$ pip3 install -r requirements.txt
```
