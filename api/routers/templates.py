import io
from typing import Optional
from fastapi import APIRouter, Response, HTTPException
from fastapi.responses import StreamingResponse
from fpdf import FPDF
from . import aruco
import qrcode
import nanoid
from math import sin, pi

templates_options = {
    "1": {"rows": 1, "columns": 1},
    "2": {"rows": 1, "columns": 2},
    "3": {"rows": 1, "columns": 3},
    "4": {"rows": 2, "columns": 2},
    "6": {"rows": 2, "columns": 3},
    "8": {"rows": 2, "columns": 4},
    "9": {"rows": 3, "columns": 3},
    "10": {"rows": 2, "columns": 5},
    "12": {"rows": 3, "columns": 4},
    "14": {"rows": 2, "columns": 7},
    "16": {"rows": 4, "columns": 4},
    "18": {"rows": 3, "columns": 6},
    "21": {"rows": 3, "columns": 7},
    "24": {"rows": 3, "columns": 8},
    "28": {"rows": 4, "columns": 7},
    "32": {"rows": 4, "columns": 8},
    "36": {"rows": 6, "columns": 6},
    "35": {"rows": 5, "columns": 7},
    "42": {"rows": 6, "columns": 7},
    "48": {"rows": 8, "columns": 8},
    "64": {"rows": 8, "columns": 8},
    "72": {"rows": 8, "columns": 9},
    "81": {"rows": 9, "columns": 9}
}


class PDF(FPDF):
    def header(self):
        # Logo
        # self.image('logo_pb.png', 10, 8, 33)
        self.image(aruco.get_aruco_mark_image(self.fields), x=5, y=5, w=15)
        self.image(aruco.get_aruco_mark_image(100), x=self.w - 20, y=5, w=15)

        self.set_font('helvetica', '', 10)
        self.set_xy(x=25, y=5)
        self.cell(w=80, h=5, txt="Nombre:",
                  ln=1, center=False, align="L", border=0)
        self.set_line_width(0.05)
        self.set_draw_color(r=200, g=200, b=200)
        self.rect(x=42, y=5,
                  w=70, h=15, style="D")

        self.set_xy(x=self.w-180, y=5)
        self.cell(w=80, h=5, txt="Título:",
                  ln=1, center=False, align="L", border=0)
        self.rect(x=self.w - 165, y=5,
                  w=138, h=15, style="D")
        # helvetica bold 15

        # # Move to the right
        # self.cell(80)

        # # Line break
        # self.ln(20)

    # Page footer

    def footer(self):
        # Position at 1.5 cm from bottom
        self.image(aruco.get_aruco_mark_image(101), x=5, y=self.h - 20, w=15)
        self.image(aruco.get_aruco_mark_image(102),
                   x=self.w - 20, y=self.h - 20, w=15)
        self.set_y(-15)
        # helvetica italic 8
        self.set_font('helvetica', 'I', 8)
        # Page number
        user_uuid = nanoid.generate(size=5)

        link = "https://aruco.escuelaideo.edu.es/u/"  # %s" % user_uuid
        img = qrcode.make(link)
        self.image(img.get_image(), x=30, y=self.h - 22, w=20)
        self.set_xy(x=50, y=self.h-12)
        self.multi_cell(w=65, h=4, align='L',
                        txt='Puedes subir la imagen a %s' % link)
        # Title
        self.set_font('helvetica', 'B', 15)
        self.set_xy(x=self.w-105, y=self.h-10)
        self.cell(w=80, h=5, txt=self.new_title,
                  ln=1, center=False, align="R", border=0)

        self.set_font('helvetica', '', 14)
        self.set_xy(x=self.w-180, y=self.h-10)
        self.cell(w=80, h=5, txt="No pintes sobre las marcas de las esquinas.",
                  ln=1, center=False, align="C", border=0)


router = APIRouter()


@router.get("/template", tags=["templates"])
async def get_template_options():
    return templates_options


@router.get("/template/{fields}", tags=["templates"])
async def generate_template(fields=4, reference: Optional[bool] = True, demo: Optional[bool] = False):
    try:
        selected_option = templates_options[fields]
    except:
        raise HTTPException(status_code=404, detail="Item not found")

    r = selected_option["rows"]
    c = selected_option["columns"]

    pdf = PDF(orientation="L")
    pdf.fields = int(fields)
    pdf.new_title = "%sx%s: %s casillas" % (r, c, (r*c))
    # pdf.new_title = "%s %s %s" % (pdf.new_title, reference, demo)
    pdf.add_page()
    # pdf.rect()

    fx = 5
    fy = 25
    fw = pdf.w - fx*2
    fh = pdf.h - fy*2

    def drawGrid(rows, columns):
        xgap = fw/columns
        ygap = fh/rows
        pdf.set_line_width(0.05)
        pdf.set_draw_color(r=200, g=200, b=200)
        pdf.set_fill_color(r=240, g=240, b=240)
        fields = rows * columns

        for r in range(rows):
            for c in range(columns):
                x1 = fx + (c*xgap)
                y1 = fy + (r*ygap)
                pdf.rect(x=x1, y=y1,
                         w=xgap, h=ygap, style="D")
                cx = x1+xgap/2
                cy = y1+ygap/2

                if demo:
                    index = r * columns + c
                    cr = min(xgap, ygap) * 2/3
                    cr = cr * sin((pi*index)/(fields))
                    pdf.set_fill_color(r=122, g=165, b=240)
                    pdf.circle(x=cx - cr/2,
                               y=cy - cr/2, r=cr, style="F")

                if reference:
                    pdf.set_fill_color(r=240, g=240, b=240)
                    pdf.circle(x=cx - 0.25,
                               y=cy - 0.25, r=0.5, style="F")

    r = selected_option["rows"]
    c = selected_option["columns"]
    drawGrid(rows=r, columns=c)

    return StreamingResponse(io.BytesIO(pdf.output()), media_type="application/pdf")
