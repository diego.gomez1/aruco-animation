import io
import os
from fastapi import APIRouter, Form, File, UploadFile
from fastapi.responses import StreamingResponse, FileResponse
import numpy as np
import cv2
import PIL
from cv2 import aruco
import sys
import imageio
from pygifsicle import optimize
from . import templates
from tempfile import TemporaryDirectory, NamedTemporaryFile

aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_250)

router = APIRouter()


@router.get("/aruco/{mark_id}", tags=["cv"])
async def generate_aruco_mark(mark_id):
    tag = np.zeros((100, 100, 1), dtype="uint8")
    cv2.aruco.drawMarker(aruco_dict, int(mark_id), 100, tag, 1)
    img_bytes = cv2.imencode('.jpg', tag)[1].tobytes()
    return StreamingResponse(io.BytesIO(img_bytes), media_type="image/jpg")


def get_aruco_mark_image(mark_id):
    tag = np.zeros((100, 100, 1), dtype="uint8")
    cv2.aruco.drawMarker(aruco_dict, int(mark_id), 100, tag, 1)
    img_bytes = cv2.imencode('.jpg', tag)[1].tobytes()
    return io.BytesIO(img_bytes)


@router.post("/aruco", tags=["cv"])
async def process_file(file: UploadFile = File(...), fps: int = Form(...)):
    contents = await file.read()

    processor = ImageProcessor(fps=fps)
    gifbuf = processor.process(contents)
    gifbuf.seek(0)
    # gifbytes = gifbuf.getvalue()

    # return StreamingResponse(gifbytes, media_type="image/gif", filename="animation.gif")
    with NamedTemporaryFile(mode="w+b", suffix=".gif", delete=False) as FOUT:
        FOUT.write(gifbuf.getvalue())
        return FileResponse(FOUT.name, media_type="image/gif")

    #frame = processor.process(contents)

    # res, im_png = cv2.imencode(".png", frame)
    # return StreamingResponse(io.BytesIO(im_png.tobytes()), media_type="image/jpeg")


class ImageProcessor:

    def __init__(self, fps=10) -> None:
        self.fps = fps

    def process(self, image_blob):
        nparr = np.fromstring(image_blob, np.uint8)
        image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        return self._processImage(image)

    def _processImage(self, image):
        [frame, sheet_id] = self._extractFrame(image)
        # return [frame, sheet_id]
        frames = self._splitFrame(frame, sheet_id)
        gifBuf = self._get_gif_from_frames(frames)
        gifBuf.seek(0)
        return gifBuf

    def _get_gif_from_frames(self, frames):
        gif = None

        with TemporaryDirectory('gif') as temp_directory:
            save_file_path = os.path.join(
                temp_directory, "generated.gif")
            fps = ('%s' % self.fps)
            with imageio.get_writer(save_file_path, format='GIF-PIL', mode='I', fps=int(fps), subrectangles=True) as writer:
                for frame in frames:
                    res, im_jpg = cv2.imencode(".jpg", frame)
                    writer.append_data(imageio.imread(im_jpg.tobytes()))
            optimize(save_file_path)
            with open(save_file_path, 'rb') as fh:
                gif = io.BytesIO(fh.read())

        return gif

    def _splitFrame(self, frame, sheet_id):
        option = templates.templates_options[str(sheet_id)]
        rows = option["rows"]
        columns = option["columns"]
        [sh, sw, _] = frame.shape
        w = 2970-100
        h = 2100 - (2*150) - (2*50) - (2*50)
        print("w: %s - sw: %s" % (w, sw))
        print("h: %s - sh: %s" % (h, sh))
        xgap = sw/columns
        print("width: %s times %s is %s" % (columns, xgap, (columns*xgap)))
        ygap = sh/rows

        frames = []

        for r in range(rows):
            xidx = 1
            for c in range(columns):
                im = frame.copy()
                x1 = int(xgap * c)
                y1 = int(ygap * r)
                x2 = int(x1 + xgap)
                y2 = int(y1 + ygap)
                xc = int(x1 + xgap/2)
                yc = int(y1 + ygap/2)
                # im[yc-2:yc+2, xc-2:xc+2] = (0, 0, 255)
                new_frame = im[y1:y2, x1:x2].copy()
                [nf_h, nf_w, _] = new_frame.shape
                # Remove 2 pixels from every border
                cropx = 2
                cropy = 2
                new_frame = new_frame[cropy:nf_h -
                                      cropy, cropx:nf_w-cropx].copy()
                frames.append(new_frame)

        return frames

    def _extractFrame(self, image):
        """
        Uses the reference configuration to align the image using the ArUco markers.
        """

        dictionary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_4X4_1000)
        parameters = cv2.aruco.DetectorParameters_create()

        # detect the markers in the image
        markerCorners, markerIds, rejectedCandidates = cv2.aruco.detectMarkers(
            image, dictionary, parameters=parameters)

        arMarkersIds = [id[0] for id in markerIds]
        print("Detected %s markers: %s" %
              (len(markerIds), ", ".join(str(v) for v in arMarkersIds)))
        [realWorldMarkerCorners, realWorldFrameBoardCorners] = self._getReferenceMarkers(
            ids=arMarkersIds)

        # tips from crackwitz (ocv forum)
        realWorldMarkerCorners.shape = (-1, 1, 2)
        realWorldFrameBoardCorners.shape = (-1, 1, 2)

        # dest points
        pts_dst = []

        for markerCorner in markerCorners:
            for i in range(4):
                pts_dst += [np.squeeze(markerCorner)[i]]

        pts_dst_m = np.asarray(pts_dst)

        # 1st homography : get matrix1 for “real world markers” to “image markers”
        matrix1, status = cv2.findHomography(realWorldMarkerCorners, pts_dst_m)

        # get the projection of “my real world frame" to “image world frame"
        imagePointsABCD = cv2.perspectiveTransform(
            realWorldFrameBoardCorners, matrix1)

        # source points
        pts_src = [[0, 0], [image.shape[1], 0], [
            image.shape[1], image.shape[0]], [0, image.shape[0]]]
        pts_src_m = np.asarray(pts_src)

        # 2nd homography : get matrix2 for “my picture to project” to “image world frame"
        matrix2, status = cv2.findHomography(
            np.asarray(imagePointsABCD), pts_src_m)

        # warp source image to destination based on homography
        warped_image = cv2.warpPerspective(
            image, matrix2, (image.shape[1], image.shape[0]))

        targetSize = realWorldFrameBoardCorners[2][0] - \
            realWorldFrameBoardCorners[0][0]
        # print("Target %s" % (targetSize))
        warped_image = cv2.resize(
            warped_image, (int(targetSize[0]), int(targetSize[1])))

        sheet_id = self._getSheetId(arMarkersIds)

        # return the processed image
        return [warped_image, sheet_id]

    def _getReferenceMarkers(self, ids=[0, 1, 2, 3]):

        def getBorderFromMarker(marker):
            x = marker["x"]
            y = marker["y"]
            height = marker["height"]
            width = marker["width"]
            return np.float32([
                [x, y],
                [x + width, y],
                [x + width, y + height],
                [x, y + height]
            ])

        filteredMarkers = self._getFilteredRealWorldMarkers(ids)

        realWorldMarkerCorners = np.empty(shape=[0, 2])
        for marker in filteredMarkers:
            realWorldMarkerCorners = np.append(
                realWorldMarkerCorners,
                getBorderFromMarker(marker), axis=0)

        topLeft = self.frame_section['topLeft']
        bottomRight = self.frame_section['bottomRight']

        realWorldFrameBoardCorners = np.float32(
            [
                [topLeft["x"], topLeft["y"]],
                [bottomRight["x"], topLeft["y"]],
                [bottomRight["x"], bottomRight["y"]],
                [topLeft["x"], bottomRight["y"]],
            ])

        return [realWorldMarkerCorners, realWorldFrameBoardCorners]

    def _getSheetId(self, ids):
        sheet_id = -1
        for id in ids:
            id = int(id)
            if id not in [100, 101, 102]:
                sheet_id = id
        return sheet_id

    def _getFilteredRealWorldMarkers(self, ids):
        filteredMarkers = []
        for id in ids:
            id = int(id)
            if id == 100:
                filteredMarkers.append(self.markers["topRight"])
            elif id == 101:
                filteredMarkers.append(self.markers["bottomLeft"])
            elif id == 102:
                filteredMarkers.append(self.markers["bottomRight"])
            else:
                filteredMarkers.append(self.markers["topLeft"])

        return filteredMarkers

    frame_section = {
        "topLeft": {"x": 50, "y": 50 + 150 + 50},
        "bottomRight": {"x": 2970 - 50, "y": 2100 - 50 - 150 - 50}
    }

    markers = {
        "topLeft": {
            "x": 50,
            "y": 50,
            "height": 150,
            "width": 150
        },
        "topRight": {
            "x": 2970 - 50 - 150,
            "y": 50,
            "height": 150,
            "width": 150
        },
        "bottomLeft": {
            "x": 50,
            "y": 2100 - 50 - 150,
            "height": 150,
            "width": 150
        },
        "bottomRight": {
            "x": 2970 - 50 - 150,
            "y": 2100 - 50 - 150,
            "height": 150,
            "width": 150
        },
    }
