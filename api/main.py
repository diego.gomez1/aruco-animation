import os
from typing import Optional
from routers import templates
from routers import aruco

from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import RedirectResponse, FileResponse, HTMLResponse

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3000",
    "http://localhost:3000/",
    "http://localhost:40125",
    "http://localhost:40125/"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(templates.router,
                   prefix="/api",)
app.include_router(aruco.router,
                   prefix="/api",)


@app.get("/api/.*", status_code=404, include_in_schema=False)
def invalid_api():
    return None


app.mount("/static", StaticFiles(directory="dist"), name="static")


@app.get("/", response_class=FileResponse)
def read_index(request: Request):
    path = 'dist/index.html'
    return FileResponse(path)


@app.get("/{catchall:path}", response_class=FileResponse)
def read_index(request: Request):
    # check first if requested file exists
    path = request.path_params["catchall"]
    file = "dist/"+path

    # print('look for: ', path, file)
    if os.path.exists(file):
        return FileResponse(file)

    # otherwise return index files
    index = 'dist/index.html'
    return FileResponse(index)
