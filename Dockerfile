FROM node:14-alpine3.14 as vuebuilder

WORKDIR /vue/src/
COPY ./client/. ./
RUN npm install
RUN npm run build

# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.8-slim-buster
RUN apt-get update
RUN apt install -y libgl1-mesa-glx libglib2.0-0 gifsicle
RUN apt clean

EXPOSE 5000

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
COPY ./api/requirements.txt .
RUN python -m pip install -r requirements.txt

WORKDIR /app
COPY ./api/. /app
COPY --from=vuebuilder /vue/src/dist ./dist

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-python-configure-containers
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["gunicorn", "--bind", "0.0.0.0:5000", "-k", "uvicorn.workers.UvicornWorker", "main:app"]
