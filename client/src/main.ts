import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";

import App from "./App.vue";
import Welcome from "./components/Welcome.vue";
import GenerateTemplate from "./components/GenerateTemplate.vue";
import ProcessTemplate from "./components/ProcessTemplate.vue";
import NotFoundComponent from "./components/NotFoundComponent.vue";

import "./index.css";

const routes = [
  { path: "/", component: Welcome },
  { path: "/u", component: ProcessTemplate },
  { path: "/download", component: GenerateTemplate },
  {
    path: "/:catchAll(.*)",
    component: NotFoundComponent,
    name: "NotFound",
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes, // short for `routes: routes`
});

const app = createApp(App);

app.use(router);

app.mount("#app");
